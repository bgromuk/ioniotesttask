import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

public class BufferedReader {

    public static final String TEST_CSV_PATH = "src/main/resources/testdata.csv";

    public static void main(String[] args) {
        try {

            try(BufferedInputStream in = new BufferedInputStream(new FileInputStream(TEST_CSV_PATH))) {
                byte[] bbuf = new byte[1024];
                while ((in.read(bbuf)) != -1) {
                    String str = new String(bbuf);
                    System.out.println(str);
                }
            }

            System.out.println("--------------------------------------------------------------------------------");

            FileReader reader = new FileReader(TEST_CSV_PATH);

            try (java.io.BufferedReader in = new java.io.BufferedReader(reader)) {
                String line;
                while ((line = in.readLine()) != null) {
                    System.out.println(line);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
