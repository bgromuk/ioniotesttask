import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class LoadWholeFile {
    public static void main(String[] args) {
        try {
            String str = new String(Files.readAllBytes(Paths.get("src/main/resources/testdata.csv")), StandardCharsets.UTF_8);
            System.out.println(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
