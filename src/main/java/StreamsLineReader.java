import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class StreamsLineReader {

    public static final String TEST_CSV_PATH = "src/main/resources/testdata.csv";

    public static void main(String[] args) {
        try {

            Files.lines(Paths.get(TEST_CSV_PATH)).forEach(str -> System.out.println(str));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
