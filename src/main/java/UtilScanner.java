import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class UtilScanner {
    public static void main(String[] args) {
        try {
            try(Scanner scanner = new Scanner(new File("src/main/resources/testdata.csv"))) {
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    System.out.println(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
